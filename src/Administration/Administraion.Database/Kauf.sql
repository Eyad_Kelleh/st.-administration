﻿CREATE TABLE [dbo].[Kauf]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY, 
    [ArtikelId] INT NOT NULL, 
    [Mamge] INT NOT NULL, 
    CONSTRAINT [FK_Kauf_Artikel] FOREIGN KEY (ArtikelId) REFERENCES Artikel(Id)
)
