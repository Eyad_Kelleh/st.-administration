﻿CREATE TABLE [dbo].[Artikel]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Preis] DECIMAL NOT NULL, 
    [HerstellerId] INT NOT NULL, 
    CONSTRAINT [FK_Artikel_Hersteller] FOREIGN KEY (HerstellerId) REFERENCES Hersteller(Id)
)
