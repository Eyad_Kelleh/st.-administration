﻿CREATE TABLE [dbo].[Bestellung]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY, 
    [PersonId] INT NOT NULL, 
    [KaufId] INT NOT NULL, 
    [VerkaufId] INT NOT NULL, 
    [Gesamtpreis] INT NOT NULL, 
    CONSTRAINT [FK_Bestellung_Person] FOREIGN KEY (PersonId) REFERENCES Person(id), 
    CONSTRAINT [FK_Bestellung_Kauf] FOREIGN KEY (KaufId) REFERENCES Kauf(Id), 
    CONSTRAINT [FK_Bestellung_VarkaufId] FOREIGN KEY (VerkaufId) REFERENCES Verkaufer(Id)
)
