﻿CREATE TABLE [dbo].[Person]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY , 
    [Geschlecht] NVARCHAR(50) NOT NULL, 
    [Vorname] NVARCHAR(50) NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [StadtId] INT NOT NULL, 
    [Straße] NVARCHAR(50) NOT NULL, 
    [StraßeNummer] INT NOT NULL, 
    [GebrustagDatum] DATE NOT NULL, 
    CONSTRAINT [FK_Person_ToTable] FOREIGN KEY (StadtId) REFERENCES Stadt(id)
)
