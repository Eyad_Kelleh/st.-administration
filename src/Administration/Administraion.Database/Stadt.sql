﻿CREATE TABLE [dbo].[Stadt]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY, 
    [PLZ] INT NOT NULL, 
    [Name] NCHAR(10) NOT NULL, 
    [LandId] INT NOT NULL, 
    CONSTRAINT [FK_Stadt_Land] FOREIGN KEY (Landid) REFERENCES Land(Id)
)
