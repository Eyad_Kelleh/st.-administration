﻿CREATE TABLE [dbo].[Verkaufer]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Vorname] NVARCHAR(50) NOT NULL, 
    [LandId] INT NOT NULL, 
    CONSTRAINT [FK_Verkaufer_Land] FOREIGN KEY (LandId) REFERENCES Land(Id)
)
