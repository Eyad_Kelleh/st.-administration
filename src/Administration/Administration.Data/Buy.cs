﻿
namespace Administration.Data
{
    public class Buy
    {
        private int _idBuy;
        private int _articalId;
        private decimal _mange;
        public int IdBuy    
        {
            get { return _idBuy; }
            set { _idBuy = value; }
        }

        public int ArticalId
        {
            get { return _articalId; }
            set { _articalId = value; }
        }
        public decimal Mange
        {
            get { return _mange; }
            set { _mange = value; }
        }
    }
}
