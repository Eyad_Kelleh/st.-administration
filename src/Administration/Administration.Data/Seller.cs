﻿namespace Administration.Data
{
    public class Seller
    {
        private int _idSeller;
        private string _firstname;
        private int _countryid;
        private string _lastname;
        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }
        public int IdSeller
        {
            get { return _idSeller; }
            set { _idSeller = value; }
        }
        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }
        public int Countryid
        {
            get { return _countryid; }
            set { _countryid = value; }
        }
    }
}
