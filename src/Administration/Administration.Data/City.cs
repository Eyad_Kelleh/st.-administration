﻿namespace Administration.Data
{
  public  class City
    {
        private int _idcity;
        private string _cityname;
        private int _plz;
        public int idCity
        {
            get { return _idcity; }
            set { _idcity = value; }
        }

        public string cityName
        {
            get { return _cityname; }
            set { _cityname = value; }
        }


        public int Plz
        {
            get { return _plz; }
            set { _plz = value; }
        }

    }
}
