﻿namespace Administration.Data
{
   public class Manafacture
    {
        private int _idManafacture;
        private string _manafacturename;
        public int Idmanafacture
        {
            get { return _idManafacture; }
            set { _idManafacture = value; }
        }
        public string Manafacurename
        {
            get { return _manafacturename; }
            set { _manafacturename = value; }
        }
    }
}
