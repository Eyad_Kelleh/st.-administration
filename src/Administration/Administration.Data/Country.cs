﻿namespace Administration.Data
{
   public class Country
    {
        private int _idCountry;
        private string _countryname;


        public int idCountry
        {
            get { return _idCountry; }
            set { _idCountry = value;}
        }

        public string countryname
        {
            get { return _countryname; }
            set { _countryname = value; }
        }
      

    }
}
