﻿namespace Administration.Data
{
   public class Artical
    {
        private int _idArticel;
        private string _articalname;
        private decimal _price;
        private string _idcountry;
        private string _manufactureByArtical;
        public int IdArtical
        {
            get { return _idArticel; }
            set { _idArticel = value; }
        }
        public string ArticalName
        {
            get { return _articalname; }
            set { _articalname = value; }
        }
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string Manafacture
        {
            get { return _manufactureByArtical; }
            set { _manufactureByArtical = value; }
        }
        public string CountyByManufacture
        {
            get { return _idcountry; }
            set { _idcountry = value; }
        }
    }
}
    