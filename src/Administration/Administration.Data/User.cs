﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Administration.Data
{
   public class User
    {
        private int _iduser;
        private string _firstname;
        private string _lastname;
        private int _plz;
        private string _city;
        private string _street;
        private int _streetnamber;
        private string _country;
        public int IdUser
        {
            get { return _iduser; }
            set { _iduser = value; }
        }
        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }
        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }
        public int Plz
        {
            get { return _plz; }
            set { _plz = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }
        public int Streetnamber
        {
            get { return _streetnamber; }
            set { _streetnamber = value; }
        }            
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

    }
}
