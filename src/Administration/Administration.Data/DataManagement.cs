﻿using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Windows;

namespace Administration.Data
{
    public class DataManagement
    {
        public SqlConnection GetsqlConnection()
        {
            var conn = new SqlConnection("user id=" +
            System.Security.Principal.WindowsIdentity.GetCurrent().Name + ";"
            + "password=;server=" + Environment.MachineName + "\\" + "SQLEXPRESS;"
            + "Trusted_Connection=yes;"
            + "database=DB_Administration;"
            + "connection timeout=30");
            conn.Open();
            return conn;
        }
        public void SetUser(string sex,string Firstname, string Lastname, int idCity, string Street, int streetNamber, string bithdayDateTime)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Person]([Geschlecht],[Vorname],[Name],[StadtId],[Straße],[StraßeNummer],[GebrustagDatum]) VALUES('" + sex + "','" + Firstname + "','" + Lastname + "','" + idCity + "','" + Street + "','" + streetNamber + "','" + bithdayDateTime + "'); ";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }

            }
        }
        public ObservableCollection<Country> GetCountries(ObservableCollection<Country> countries)
        {

            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT[Id],[LandName] FROM[DB_Administration].[dbo].[Land]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Country element = new Country();
                    element.idCountry = int.Parse(reader["Id"].ToString());
                    element.countryname = reader["LandName"].ToString();
                    countries.Add(element);
                }
                return countries;
            }
        }

        public void GetBuy()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<City> GetCities(ObservableCollection<City> cities, int idland)
        {
            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT[Id],[PLZ],[Name],[LandId] FROM[DB_Administration].[dbo].[Stadt] WHERE LandId ='" + idland + "' ";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    City element = new City();
                    element.idCity = int.Parse(reader["Id"].ToString());
                    element.cityName = reader["Name"].ToString();
                    element.Plz = int.Parse(reader["PLZ"].ToString());
                    cities.Add(element);
                }
                return cities;
            }
        }
        public ObservableCollection<User> GetUser(ObservableCollection<User> users)
        {
            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText =
                    "SELECT[Id],[Geschlecht],[Vorname],[Name],[PLZ],[Expr1],[Straße],[StraßeNummer],[GebrustagDatum],[LandName] FROM[DB_Administration].[dbo].[Perons]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User element = new User();
                    element.IdUser = int.Parse(reader["Id"].ToString());
                    element.Firstname = reader["Vorname"].ToString();
                    element.Lastname = reader["Name"].ToString();
                    element.Plz = int.Parse(reader["PLZ"].ToString());
                    element.City = reader["Expr1"].ToString();
                    element.Street = reader["Straße"].ToString();
                    element.Streetnamber = int.Parse(reader["StraßeNummer"].ToString());
                    element.Country = reader["LandName"].ToString();
                    users.Add(element);
                }
                return users;
            }
        }
        public void SetManufacture(string Nama, int IdCountry)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Hersteller] ([Name],[LandId]) VALUES('" + Nama + "'," + IdCountry + ");";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }

            }
        }
        public ObservableCollection<Manafacture> GetManafacture(ObservableCollection<Manafacture> manafactures)
        {

            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT [Id],[Name],[LandId] FROM [DB_Administration].[dbo].[Hersteller]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Manafacture element = new Manafacture();
                    element.Idmanafacture = int.Parse(reader["Id"].ToString());
                    element.Manafacurename = reader["Name"].ToString();
                    manafactures.Add(element);
                }
                return manafactures;
            }
        }
        public void SetSeller(string firstname, string lastname, int IdCountry)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Verkaufer]([Name],[Vorname],[LandId]) VALUES('" + firstname + "','" + lastname + "'," + IdCountry + "); ";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }

            }
        }
        public ObservableCollection<Seller> GetSellers(ObservableCollection<Seller> sellers)
        {

            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT[Id],[Name],[Vorname],[LandId] FROM[DB_Administration].[dbo].[Verkaufer]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Seller element = new Seller();
                    element.IdSeller = int.Parse(reader["Id"].ToString());
                    element.Firstname = reader["Vorname"].ToString();
                    element.Lastname = reader["Name"].ToString();
                    element.Countryid = int.Parse(reader["LandId"].ToString());
                    sellers.Add(element);
                }
                return sellers;
            }
        }
        public void setArticel(int idmanafacture, string articelname, int price)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Artikel]([Name],[Preis],[HerstellerId]) VALUES('" + articelname + "'," + price + "," + idmanafacture + "); ";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }
            }
        }
        public ObservableCollection<Artical> GetArtical(ObservableCollection<Artical> articals)
        {

            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT [Id],[Name],[Preis],[Hersteller],[LandName] FROM [dbo].[ArtikelbeiHersteller]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Artical element = new Artical();
                    element.IdArtical = int.Parse(reader["Id"].ToString());
                    element.ArticalName = reader["Name"].ToString();
                    element.Price = decimal.Parse(reader["Preis"].ToString());
                    element.Manafacture = reader["Hersteller"].ToString();
                    element.CountyByManufacture = reader["LandName"].ToString();
                    articals.Add(element);
                }
                return articals;
            }
        }
        public void SetBuy(int idArtical, int mange)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Kauf]([ArtikelId],[Mamge]) VALUES('" + idArtical + "'," + mange + "); ";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }
            }
        }
        public ObservableCollection<Buy> GetBuy(ObservableCollection<Buy> buys)
        {

            using (var conn = GetsqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT [Id],[ArtikelId],[Mamge] FROM [DB_Administration].[dbo].[Kauf]";
                cmd.Connection = conn;
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Buy element = new Buy();
                    element.IdBuy = int.Parse(reader["Id"].ToString());
                    element.ArticalId = int.Parse(reader["ArtikelId"].ToString());
                    element.Mange = decimal.Parse(reader["Mamge"].ToString());
                    buys.Add(element);
                }
                return buys;
            }
        }
        public void SetOrder(int idUser, int idBuy, int idSeller, decimal amount)
        {
            using (var conn = GetsqlConnection())
            {
                string Sql_INSERT = "INSERT INTO [dbo].[Bestellung]([PersonId],[KaufId],[VerkaufId],[Gesamtpreis])VALUES(" + idUser + "," + idBuy + "," + idSeller + "," + amount + "); ";
                SqlCommand cmd = new SqlCommand(Sql_INSERT, conn);
                try
                {
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
                catch (SqlException)
                {
                    MessageBox.Show("INVALID INSERTION!, COMPLETE DATA USING THE CORRECT TYPE OF DATA IN THE FIELDS!", "ALERT");
                }
            }
        }
    }
}
