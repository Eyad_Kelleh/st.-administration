﻿using System.Windows;
using Administration.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Windows.Controls;

namespace Administration
{
    public partial class ControlNewUser : INotifyPropertyChanged
    {
        private DataManagement dm = new DataManagement();
        private ObservableCollection<Country> _listCountry = new ObservableCollection<Country>();
        private ObservableCollection<City> _listcities = new ObservableCollection<City>();
        public ObservableCollection<Country> ListCountry 
        {
            get { return _listCountry; }
            set
            {
                _listCountry = value;
                OnPropertyChanged(nameof(ListCountry));
            }
        }

        public ObservableCollection<City> ListCities
        {
            get { return _listcities; }
            set
            {
                _listcities = value;
                OnPropertyChanged(nameof(ListCities));
            }
        }
  
        public ControlNewUser()
        {
            InitializeComponent();
            DataContext = this;
            dm.GetCountries(ListCountry);
            CboCountry.SelectionChanged += new SelectionChangedEventHandler (CboCountry_SelectionChanged);
    }

        private void CboCountry_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CboCities.SelectedIndex = CboCountry.SelectedIndex;
            var idland = ((Country)CboCountry.SelectedItem).idCountry;
            dm.GetCities(ListCities,idland);         
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            string fristname = TxtFirstname.Text;
            string lastname = Txtlastname.Text;
            int PLZ = ((City)CboPlz.SelectedItem).Plz;
            int idcity = ((City)CboCities.SelectedItem).idCity;
            string street = TxtStreet.Text;
            int streetnameber = int.Parse(TxtStreetNamber.Text) ;
            string country = ((Country)CboCountry.SelectedItem).countryname;
            string sex = TxtSex.Text;
            string brithday = TxtBrithday.Text;
            dm.SetUser(sex,fristname, lastname, idcity, street, streetnameber, brithday);
            MessageBox.Show("The user is saved"+fristname.ToString()+ lastname.ToString());
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

      
    }
}
