﻿using Administration.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace Administration
{
    /// <summary>
    /// Interaction logic for ControlSeller.xaml
    /// </summary>
    public partial class ControlSeller : INotifyPropertyChanged
    {
        private DataManagement dm = new DataManagement();
        private ObservableCollection<Country> _listCountry = new ObservableCollection<Country>();
        public ObservableCollection<Country> ListCountry
        {
            get { return _listCountry; }
            set
            {
                _listCountry = value;
                OnPropertyChanged(nameof(ListCountry));
            }
        }
        public ControlSeller()
        {
            InitializeComponent();
            DataContext = this;
            dm.GetCountries(ListCountry);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            int idCountry = CboCountry.SelectedIndex + 1;
            string firstname = TxtFirstname.Text;
            string lastname = Txtlastname.Text;
            dm.SetSeller(firstname,lastname,idCountry);
            MessageBox.Show("Saved " + firstname.ToString() + " " + lastname.ToString());
        }
    }
}
