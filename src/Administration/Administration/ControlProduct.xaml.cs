﻿using Administration.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Administration
{
    public partial class ControlProduct : INotifyPropertyChanged
    {
        private DataManagement dm = new DataManagement();
        private ObservableCollection<Country> _listCountry = new ObservableCollection<Country>();
        private  ObservableCollection<Manafacture> _manafactures = new ObservableCollection<Manafacture>();
        public ObservableCollection<Manafacture> ListManafactures
        {
            get { return _manafactures; }
            set
            {
                _manafactures = value;
                OnPropertyChanged(nameof(ListManafactures));
            }
        }
        public ObservableCollection<Country> ListCountry
        {
            get { return _listCountry; }
            set
            {
                _listCountry = value;
                OnPropertyChanged(nameof(ListCountry));
            }
        }
        public ControlProduct()
        {
            InitializeComponent();
            DataContext = this;
            dm.GetCountries(ListCountry);
            dm.GetManafacture(ListManafactures);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void Save_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            int idCountry = CboCountry.SelectedIndex + 1;
            string manafacturename = TxtName.Text;
            dm.SetManufacture(manafacturename, idCountry);
            dm.GetManafacture(ListManafactures);
            int idmanfacure = ListManafactures.LastOrDefault().Idmanafacture;
            MessageBox.Show("Saved " + manafacturename.ToString());
        }
        private void SaveArticel_Checked(object sender, RoutedEventArgs e)
        {
            var idCurrentManafacture = ((Manafacture)CboManafactur.SelectedItem).Idmanafacture;
            string articelName = TxtArticelname.Text;
            int price = int.Parse(TxtPrice.Text);
            dm.setArticel(idCurrentManafacture, articelName, price);
            MessageBox.Show("Saved " + articelName.ToString());
        }
    }
}
