﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using Administration.Data;

namespace Administration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private DataManagement dm = new DataManagement();
        private ObservableCollection<User> _listUsers = new ObservableCollection<User>();
        private ObservableCollection<Seller> _listSeller = new ObservableCollection<Seller>();
        public ObservableCollection<User> ListUsers
        {
            get { return _listUsers; }
            set
            {
                _listUsers = value;
                OnPropertyChanged(nameof(ListUsers));
            }
        }
        public ObservableCollection<Seller> ListSellers
        {
            get { return _listSeller; }
            set
            {
                _listSeller = value;
                OnPropertyChanged(nameof(ListSellers));
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            dm.GetUser(ListUsers);
            dm.GetSellers(ListSellers);
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void NewUser_Click(object sender, RoutedEventArgs e)
        {
            ControlNewUser newUser = new ControlNewUser();
            newUser.ShowDialog();
            //Flyout.IsOpen = true;
        }
        private void Product_Click(object sender, RoutedEventArgs e)
        {
            var product = new ControlProduct();
            product.ShowDialog();
        }
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void BtnRefrech_Checked(object sender, RoutedEventArgs e)
        {
            ListUsers.Clear();
            dm.GetUser(ListUsers);
        }
        private void Seller_Click(object sender, RoutedEventArgs e)
        {
            var seller = new ControlSeller();
            seller.ShowDialog();
        }
        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            var buy = new ControlBuy();
            buy.ShowDialog();
        }
    }
  
}
