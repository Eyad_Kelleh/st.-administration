﻿using System;
using Administration.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
namespace Administration
{
   public partial class ControlBuy : INotifyPropertyChanged
    {
        private DataManagement dm = new DataManagement();
        private ObservableCollection<Artical> _articals = new ObservableCollection<Artical>();
        private ObservableCollection<User> _users = new ObservableCollection<User>();
        private ObservableCollection<Buy> _buys = new ObservableCollection<Buy>();
        private ObservableCollection<Seller> _sellers = new ObservableCollection<Seller>();
        public ControlBuy()
        {
            InitializeComponent();
            DataContext = this;
            dm.GetUser(ListUsers);
            dm.GetArtical(ListArtical);
            dm.GetSellers(Sellers);
        }

        public ObservableCollection<User> ListUsers
        {
            get { return _users; }
            set
            {
                _users = value;
                OnPropertyChanged(nameof(ListUsers));
            }
        }
        public ObservableCollection<Artical> ListArtical
        {
            get { return _articals; }
            set
            {
                _articals = value;
                OnPropertyChanged(nameof(ListArtical));
            }
        }
        public ObservableCollection<Buy> Buys
        {
            get { return _buys; }
            set
            {
                _buys = value;
                OnPropertyChanged(nameof(Buys));
            }
        }
        public ObservableCollection<Seller> Sellers
        {
            get { return _sellers; }
            set
            {
                _sellers = value;
                OnPropertyChanged(nameof(Sellers));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Save_Checked(object sender, RoutedEventArgs e)
        {
            CreateABuy(out var idartical, out var mange);
            CreateOrder(mange);
        }

        private void CreateOrder(int mange)
        {
            var buys = dm.GetBuy(Buys);
            int currentIdBuy = buys.LastOrDefault().IdBuy;
            int currentIdUser = ((User) CboAddUser.SelectedItem).IdUser;
            int currentIdSeller = ((Seller) CboSeller.SelectedItem).IdSeller;
            decimal price = ((Artical) CboArtical.SelectedItem).Price;
            decimal amount = Math.Round(mange * price);
            dm.SetOrder(currentIdUser, currentIdBuy, currentIdSeller, amount);
            MessageBox.Show("your order is confirmed!", "Successfully", MessageBoxButton.OK,MessageBoxImage.Information);
        }

        private ObservableCollection<Buy> CreateABuy(out int idartical, out int mange)
        {
            idartical = ((Artical) CboArtical.SelectedItem).IdArtical;
            mange = int.Parse(TxtPrice.Text);
            dm.SetBuy(idartical, mange);
            var aBuy = dm.GetBuy(Buys);
            return aBuy;
        }
    }
}
